#version 330

struct LightInfo
{
  	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

uniform sampler2D snowTex; // текстура снега
uniform sampler2D grassTex; // текстура травы 
uniform sampler2D sandTex; // тектсура песка
uniform sampler2D waterTex; // текстура воды
uniform sampler2D mapTex; // маска с цветами

in vec3 normalCamSpace; // Нормаль в системе координат камеры

in vec4 posCamSpace; // Координаты вершины в системе координат камеры
in vec2 texCoord; // Текстурные координаты
in vec2 mapCoord; // Координаты для карты рельефа
in vec4 lightDirCamSpace; // Направление на источник света

out vec4 fragColor; //выходной цвет фрагмента

const float shiness = 128.0; // яркость бликов

void main()
{
    // цвета текстур
    vec3 snowColor = texture(snowTex, texCoord).rgb;
    vec3 grassColor = texture(grassTex, texCoord).rgb;
    vec3 sandColor = texture(sandTex, texCoord).rgb;
    vec3 waterColor = texture(waterTex, texCoord).rgb;

    // RGBA маски
    vec4 mapColor = texture(mapTex, mapCoord).rgba;
    
    // считаем дифузионный цвет
    vec3 diffuseColor = mapColor.r * sandColor + mapColor.g * grassColor + mapColor.b * waterColor + (1 - mapColor.a) * snowColor; 
    
    vec3 normal = normalize(normalCamSpace); // Нормализуем нормаль после интерполяции
    vec3 viewDirection = normalize(-posCamSpace.xyz); // Направление на виртуальную камеру
    
    vec3 lightDirCamSpace = normalize(light.pos - posCamSpace.xyz); // Направление на источник света
    float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); // Скалярное произведение
    
    // дифузионное освещение + фоновое освещение
    vec3 color = diffuseColor * (light.La + light.Ld * NdotL);
    
    if (NdotL > 0.0) 
    {
      vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); // Биссектриса между направлением на камеру и на источник света
      
      float blinnTerm = max(dot(normal, halfVector), 0.0); // Интесивность бликового освещения по Блинну
      blinnTerm = pow(blinnTerm, shiness); // Регулируем размер блика
      // задаем интенсивность бликов, снег бликует больше всего - поэтому у него такой большой коэф (100)
      // поменьше бликует вода, остальное не бликует
      float Ks = (1 - mapColor.a) * 100 + mapColor.b * 10; 
      color += light.Ls * vec3(Ks, Ks, Ks) * blinnTerm; // Добавляем бликовую часть
    }
    fragColor = vec4(color, 1.0);
}

