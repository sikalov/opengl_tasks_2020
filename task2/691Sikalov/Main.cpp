#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.h"
#include "Texture.hpp"
#include "LightInfo.hpp"

#include <iostream>
#include <vector>

class TestApplication : public Application {
public:
    //Переменные для управления положением одного источника света
    float _lr = 3.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.35f;

    LightInfo _light;

    MeshPtr _marker; //Маркер для источника света

    //Идентификатор шейдерной программы
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

	void makeScene() override {

		Application::makeScene()
;
		_cameraMover = std::make_shared<FirstPersonCameraMover>(_mesh);

		_mesh = make_relief(2.f, 200U, 5, 1.5);
		_mesh->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
        _marker = makeSphere(0.05f);

		_shader = std::make_shared<ShaderProgram>("691SikalovData2/diffuseDirectionalLight.vert", "691SikalovData2/diffuseDirectionalLight.frag");
        _markerShader = std::make_shared<ShaderProgram>("691SikalovData2/marker.vert", "691SikalovData2/marker.frag");

        _snow = loadTexture("691SikalovData2/snow.jpg");
        _grass = loadTexture("691SikalovData2/grass.jpg");
        _sand = loadTexture("691SikalovData2/sand.jpg");
        _water = loadTexture("691SikalovData2/water.png");
        _map = loadTexture("691SikalovData2/map.png"); // mask

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
	}

    // Added just for determining where is the light
    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::SliderFloat("radius", &_lr, 0.0f, 5.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
        }
        ImGui::End();
    }

    void draw() override {

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _mesh->modelMatrix()))));

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * (float)_lr;
        _light.ambient = glm::vec3(0, 0, 0);
        _light.diffuse = glm::vec3(0.5, 0.5, 0.5);
        _light.specular = glm::vec3(0.2, 0.2, 0.2);

        glm::vec3 _lightAmbientColor; // Ambient or reflected lightning from nearest objects
        glm::vec3 _lightDiffuseColor; // Diffuse component is light coming from one direction

        _shader->setVec3Uniform("light.pos", _light.position);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _snow->bind();
        _shader->setIntUniform("snowTex", 0);

        glActiveTexture(GL_TEXTURE0 + 1);
        _grass->bind();
        _shader->setIntUniform("grassTex", 1);

        glActiveTexture(GL_TEXTURE0 + 2);
        _sand->bind();
        _shader->setIntUniform("sandTex", 2);

        glActiveTexture(GL_TEXTURE0 + 3);
        _water->bind();
        _shader->setIntUniform("waterTex", 3);

        glActiveTexture(GL_TEXTURE0 + 4);
        _map->bind();
        _shader->setIntUniform("mapTex", 4);

        _mesh->draw();

        //Рисуем маркеры для всех источников света
        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

protected:
    TerrainPtr _mesh;

    //TexturePtr _texture;
    TexturePtr _snow, _grass, _sand, _water, _map;
    GLuint _sampler;

};

int main() {
	TestApplication app;
	app.start();
	
	return 0;
}
