#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.h"

#include <iostream>
#include <vector>

class ReliefApp : public Application {
public:
	void makeScene() override {

		Application::makeScene();
		_cameraMover = std::make_shared<FreeCameraMover>();

		_mesh = make_relief(10.f, 200U, 5, 125);
		_mesh->setModelMatrix(glm::mat4(1.0f));

		_shader = std::make_shared<ShaderProgram>("691SikalovData1/shader.vert", "691SikalovData1/shader.frag");
	}

	void draw() override {
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		_shader->use();
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
		_shader->setMat4Uniform("modelMatrix", _mesh->modelMatrix());

		_mesh->draw();
	}

protected:
	MeshPtr _mesh;
	ShaderProgramPtr _shader;
};

int main() {
	ReliefApp app;
	app.start();
	
	return 0;
}
